#!/usr/bin/env python
#-*- coding:utf-8 -*-
import os
import json

def login():
    #用户注册
    account_file ='d:\login.txt'
    locked_file ='d:\lock.txt'
    print("-----------------------用户注册------------------")
    login_name = input("请输入用户名:")
    login_passwd=input("请输入密码:")
    file = open(account_file,'a')
    file.write('{'+"'user':"+"'"+login_name+"'"+',')
    file.close()
    file_ps = open(account_file, 'a')
    file_ps.write("'pass':"+"'"+login_passwd+"'"+'}'+'\n')
    print("恭喜你注册成功！")
    print('用户名：',login_name)
    print('密  码：',login_passwd)
    file_ps.close()


# 用户登录
def read_account():
    account_file = 'd:\login.txt'
    locked_file = 'd:\lock.txt'
    login_name = input("请输入用户名:")
    login_passwd = input("请输入密码:")
    read_file = open(account_file,'r')
    for line in read_file:
        read_user=dict
        read_user=eval(line)#函数转换string到dict
        read_pass=eval(line)
        a=read_user['user']
        b=read_pass['pass']
        #检查账户锁定情况
        def locked(user):
            locked_file = 'd:\lock.txt'
            read_file_lock = open(locked_file, 'a')
            read_file_lock = open(locked_file, 'r')
            for lock_line in read_file_lock:
                read_user1 = dict
                read_user1 = eval(lock_line)  # 函数转换string到dict
                f = read_user1['user']
                if f == user:
                    return 0
                else:
                    return 1
        if a!=login_name:
            print("")
            #print("该用户名不存在，请重新输入正确的用户！")
        elif a==login_name and locked(login_name)==0:
            print("该账户被锁定，请联系管理员解锁！")
            break
        else:
            if a==login_name:
                count = 1
                for i in range(10):
                    if b != login_passwd:
                        print("密码错误，请重新输入，注意：三次错误后锁定账户！")
                        login_passwd = input("请输入密码:")
                        if count < 4:
                            print('密码重试次数：',count)
                            count+=1
                            continue
                        else:
                            print("密码错误重试次数超过3次，账户被锁定！")
                            locked = open(locked_file, 'a')
                            locked.write('{' + "'user':" + "'" + login_name + "'" + '}' + '\n')
                            break

                    else:
                        print("欢迎登录成功！")
                        break


if __name__ == '__main__':
    read_account()
    #login()